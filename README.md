
# Index
- [Index](#index)
- [What is this](#what-is-this)
- [Learning Resources](#learning-resources)
- [Libraries](#libraries)
  - [Engine Architecture](#engine-architecture)
    - [Entity-Component-Systems](#entity-component-systems)
    - [Reflection](#reflection)
    - [Serialization](#serialization)
    - [Utils](#utils)
  - [Math](#math)
  - [Rendering](#rendering)
  - [Physic](#physic)
  - [Audio](#audio)
  - [Scripts](#scripts)
  - [Network](#network)
  - [Debug](#debug)
  - [Editor-imgui](#editor-imgui)
  - [Others](#others)
- [Utilities](#utilities)
- [Existing engines](#existing-engines)
- [GDC Resources for programmers](#gdc-resources-for-programmers)
  - [Rendering](#rendering-1)
  - [AI](#ai)
  - [Physics](#physics)
  - [Gameplay](#gameplay)
  - [Others](#others-1)
- [GDC Resources for designers](#gdc-resources-for-designers)
  - [Animations](#animations)
  - [Camera](#camera)
  - [UI](#ui)
  - [Design](#design)
- [Books recommendations *(programmers)*](#books-recommendations-programmers)
- [Free eBooks recommendations](#Free-eBooks-recommendations)

---
# What is this
A list of resources on Game Engine developement, gathered over the years.
Aimed at students and hobbyist interested in programming a C++ engine on their own.  
*Please hover any link to display additionnal information as a tooltip.*

<!-- ############################### LEARNING RESOURCES ############################### -->
# Learning Resources

Interesting websites, videos

* The Cherno, youtuber
  * [Cherno Engine] : Youtube playlist on how to start a new Engine in C++ from scratch. very good content  *`recommended`*
  * [Cherno C++] : Youtube playlist on C++, more as a refresher for students
  * [Cherno OpenGL]  
* [Nomad] : ECS architecture basics
* [CppCoreGuidelines] : C++ Guidelines
* [cppbestpractices] : Best Practices for C++
* [Game Programming Patterns] : Design patterns for Game Programming *`recommended`*
* [Design Pattern Catalog] : Design patterns 
* [3D Graphics] : Learning Modern 3D Graphics Programming
* [learnopengl] : Basics, the intermediate, and all the advanced knowledge using modern (core-profile) OpenGL
* [vulkantuto] : Vulkan Tutorial
* [Quaternions] : Visualizing quaternions
* [ProgrammerTricks] : A collection of (mostly) technical things every software developer should know about
* [A Flexible Reflection System in C++] : Basics of a reflection system
* [Leetcode] : Online tool to practice coding with small problems, great for learning. *`recommended`*
* [HackerRank] : Online tool to practice coding, Used by many companies for Interviews / Tech tests 
* [Git Tutorial] : Good Tutorial on everything you need to know on GiT for a small project *`recommended`*

<!-- Links  -->
[Cherno Engine]: https://youtube.com/playlist?list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT "Very good starting point for an engine."
[Cherno C++]: https://youtube.com/playlist?list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb "Basic C++ refresher"
[Cherno OpenGL]: https://youtube.com/playlist?list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2
[Nomad]:https://savas.ca/nomad "An example on how to implement ECS" 
[CppCoreGuidelines]:https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md
[cppbestpractices]:https://github.com/lefticus/cppbestpractices
[A Flexible Reflection System in C++]:https://preshing.com/20180116/a-primitive-reflection-system-in-cpp-part-1/
[3D Graphics]:https://paroj.github.io/gltut/index.html
[learnopengl]:https://learnopengl.com/
[vulkantuto]:https://vulkan-tutorial.com/
[Quaternions]:https://eater.net/quaternions
[ProgrammerTricks]:https://github.com/mtdvio/every-programmer-should-know

[Game Programming Patterns]:http://gameprogrammingpatterns.com/contents.html
[Design Pattern Catalog]:https://refactoring.guru/design-patterns/catalog

[HackerRank]:https://www.hackerrank.com/
[Leetcode]:https://leetcode.com/

[Git Tutorial]:https://www.youtube.com/playlist?list=PL4cUxeGkcC9goXbgTDQ0n_4TBzOO0ocPR


<!-- ############################### LIBRARIES ############################### -->
# Libraries

Libraries you can use in your Game Engine. If there are multiple choices, I placed a *`recommended`* tag for the one I would recommend.  


## Engine Architecture

### Entity-Component-Systems

*Architecture to handle all the objects in your game. Please make sure that you understand ECS before implementing this system, it will impact the way you write scripts / your gameplay*

* [entt] *`recommended`*
* [entityX]  
* [LIONAnt MECS]
* [corgi]

<!-- Links  -->
[entt]:https://github.com/skypjack/entt "Gaming meets modern C++ - a fast and reliable entity component system (ECS) and much more. Used in a LOT of commercial projects, including Minecraft."
[entityX]:https://github.com/alecthomas/entityx "EntityX - A fast, type-safe C++ Entity-Component system"
[LIONAnt MECS]: https://github.com/LIONant-depot/MECSV2 "Simple multi-core ECS system. Design to leverage a 21 century way of designing software. Speed, size, GPU, CPU all matters. Useful for games and any other type of realtime app."
[corgi]: https://github.com/google/corgi "C++ entity-component system"

### Reflection

*A reflection system is important for your engine to simplify serialization, Editor Entity-Inspectors, Binding with a scripting system, and so much more. Invest early in a Reflection system to avoid Hard-coding every Component property access.*

* [LIONAnt properties]
* [rttr]
* [ponder]
* [meta] EnTT reflection system, *`recommended`*
* [iguana] Reflection + Serialization (not used in commercial product)
* [MetaStuff] Simple Reflection + Serialization (not used in commercial product) 

<!-- Links  -->
[LIONAnt properties]: https://gitlab.com/LIONant/properties "C++ 17 Property System / Reflection System, Useful for games and other applications."
[rttr]: https://www.rttr.org/ "An open source library, which adds reflection to C++"
[ponder]:https://github.com/billyquith/ponder "C++ reflection library with Lua binding, and JSON and XML serialisation."
[meta]:https://github.com/skypjack/meta "Header-only, non-intrusive and macro-free runtime reflection system in C++"
[iguana]:https://github.com/qicosmos/iguana "C++17 Reflection + Serialization / Unserialization. To test"
[MetaStuff]:https://github.com/eliasdaler/MetaStuff "Reflection + Serialization / Unserialization. Embed your own josn lib. To test"

### Serialization

* [cereal]
* [rapidJson]

<!-- Links  -->
[cereal]: https://github.com/USCiLab/cereal "A C++11 library for serialization, handles JSON and Binary data"
[rapidJson]:https://github.com/Tencent/rapidjson "A fast JSON parser/generator for C++ with both SAX/DOM style API"

### Utils

* [crossGUID]  
* [uuid]
* [inih] 
* [hopscotch]  
* [xCore]
* [tweeny]

<!-- Links  -->
[crossGUID]:https://github.com/graeme-hill/crossguid "Lightweight cross platform C++ GUID/UUID library"
[inih]:https://github.com/jtilly/inih "inih (INI Not Invented Here) is a simple .INI file parser written in C."
[hopscotch]:https://github.com/Tessil/hopscotch-map "C++ implementation of a fast hash map and hash set using hopscotch hashing"
[xCore]:https://gitlab.com/LIONant/xcore "Low level C++ 17/20 Core Library for a simulation Engine : GUID generation, fast RTTI support, File access, Basic Mathematics, Multithreading, optimized collections"
[uuid]:https://github.com/wc-duck/uuid_h/blob/master/uuid.h "Single file, STB-style, to generate uuid:s."
[tweeny]: https://github.com/mobius3/tweeny "A modern C++ tweening library"

## Math

* [eigen]
* [glm]

<!-- Links  -->
[eigen]:https://gitlab.com/libeigen/eigen "Eigen is a C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms."
[glm]:https://github.com/g-truc/glm "OpenGL Mathematics (GLM)"

## Rendering

* [bgfx]  
* [Diligent Engine]  
* [meshoptimizer] *`recommended`*
* [assimp]
* [freetype2]
* [stb] 

<!-- Links  -->
[bgfx]:https://github.com/bkaradzic/bgfx "Cross-platform, graphics API agnostic, Bring Your Own Engine/Framework style rendering library."
[Diligent Engine]:https://github.com/DiligentGraphics/DiligentEngine "A modern cross-platform low-level graphics library and rendering framework"
[meshoptimizer]:https://github.com/zeux/meshoptimizer "Mesh optimization library that makes meshes smaller and faster to render"
[assimp]:https://github.com/assimp/assimp "Open Asset Import Library, The official Open-Asset-Importer-Library Repository. Loads 40+ 3D-file-formats into one unified and clean data structure."
[freetype2]:https://github.com/aseprite/freetype2 "Font rendering Library."
[stb]:https://github.com/nothings/stb "Image loader / writer / resizers / Font rasterizer"

## Physic

* [physX] `recommended`
* [Bullet3]
* [ReactPhysics3D]

<!-- Links  -->
[physX]:https://developer.nvidia.com/physx-sdk "PhysX, NVidia's physic engine, integrated in multiple game engines like Unity and Unreal. Very good library, quite intuitive to integrate"
[Bullet3]:https://github.com/bulletphysics/bullet3 "Bullet Physics SDK: real-time collision detection and multi-physics simulation for VR, games, visual effects, robotics, machine learning etc. Implements Continuous Collision Detection, perfect for small & fast moving rigidbodies, like bullets ;)"
[ReactPhysics3D]:https://github.com/DanielChappuis/reactphysics3d "Open source C++ physics engine"


## Audio

* [FMod] *`recommended`*

[FMod]:https://www.fmod.com/

## Scripts

*A Script system is useful to allow your gameplay programmers / designers / non-tech to code behaviors, game rules... the actual content of the game, without having to deal with the complexity of merging or fixing Visual Studio solutions. It is important to always provide a working version of the engine (tested and validated) to everyone that uses the script, to minimize the impact of a core bug.  
IMPORTANT: Always make sure that the script langage you chose is well understood by your whole team if your project is a short duration*

* [Mono] *`recommended`*
* [Mono Documentation] How to embed Mono
* [Mono Wrapper] Mono Wrapper to make the embedding easier
* [LuaJIT]
* [ChaiScript]

<!-- Links  -->
[Mono]:https://github.com/mono/mono "C# script, well known and used (unity script engine), see the next entry on how to embed it in your engine"
[Mono Documentation]:https://www.mono-project.com/docs/advanced/embedding/ "Documentation to embed Mono in your engine." 
[Mono Wrapper]:https://github.com/volcoma/monopp "A wrapper, to simplify the implementation of Mono as a script system for your engine." 
[LuaJIT]:https://github.com/LuaJIT/LuaJIT/ "Lua Just-In-Time, LUA script system, with acceptable performances, and used in multiple projects"
[sol2]:https://github.com/ThePhD/sol2 "Sol3 (sol2 v3.0) - a C++ <-> Lua API wrapper with advanced features and top notch performance"
[ChaiScript]:https://github.com/ChaiScript/ChaiScript/ "Chaiscript, a EMCScript derivate, easy to integrate in your engine, the syntax of the actual script requires some practice"

## Network

* [SLikeNet] Build over Raknet, *`recommended`*
* [RakNet] Raknet `! discontinued`

<!-- Links  -->
[RakNet]:https://github.com/facebookarchive/RakNet "RakNet is a cross platform, open source, C++ networking engine for game programmers."
[SLikeNet]:https://github.com/SLikeSoft/SLikeNet "SLikeNet™ is an Open Source/Free Software cross-platform network engine written in C++ and specifially designed for games (and applications which have comparable requirements on a network engine like games) building upon the discontinued RakNet network engine which had more than 13 years of active development."

## Debug

*Tracy is a must have to help you optimize your game performances. Learn how to use the profiler, add enough profile points and use the labels for automatic marks (fps goes below 60 for instance) on problematic frames.  
Spdlog is a log system, that you should use early to output debug information in file. You can troubleshoot other teammates issues by reading their log file and see what happened.*

* [Tracy]  *`MUST HAVE`*
* [SPDLog]  *`recommended`*
* [renderdoc], a standalone graphics debugging tool  
* [debug_assert] *`recommended`* instead of design your own macros
* [debug_draw]

<!-- Links  -->
[Tracy]:https://github.com/wolfpld/tracy "C++ frame profiler"
[SPDLog]:https://github.com/gabime/spdlog "Fast C++ logging library."
[renderdoc]:https://github.com/baldurk/renderdoc "RenderDoc is a stand-alone graphics debugging tool."
[debug_assert]: https://github.com/foonathan/debug_assert "Simple, flexible and modular assertion macro."
[debug_draw]: https://github.com/glampert/debug-draw "Immediate-mode, renderer agnostic, lightweight debug drawing API."


## Editor-imgui

*ImGUI is the only option here: free to use, enough widget, stable enough and well maintained.  
You could also consider an editor written WPF or WebForm, using your engine as a hotloaded DLL, but it is a verbose solution as it requires an API toward your engine and proper gameplay code abstraction.*

* [imgui]
* [imNodes]
* [imgui-node-editor]
* [ImGuizmo]
* [imGuIZMO.quat]
* [ImGui-FileDialog]
* [ImGuiAl]

<!-- Links  -->
[imgui]:https://github.com/ocornut/imgui "Dear ImGui: Bloat-free Graphical User interface for C++ with minimal dependencies"
[imNodes]:https://github.com/Nelarius/imnodes "A small, dependency-free node editor for dear imgui"
[imgui-node-editor]: https://github.com/thedmd/imgui-node-editor "Node Editor using ImGui"
[ImGuizmo]:https://github.com/CedricGuillemet/ImGuizmo "Immediate mode 3D gizmo for scene editing and other controls based on Dear Imgui"
[imGuIZMO.quat]:https://github.com/BrutPitt/imGuIZMO.quat "ImGui GIZMO widget - 3D object manipulator / orientator"
[ImGui-FileDialog]:https://github.com/gallickgunner/ImGui-Addons "Cross Platform File Dialog for Dear-ImGui"
[ImGuiAl]:https://github.com/leiradel/ImGuiAl/ "Additional Widgets for imgui"

## Others

* [recast] Navigation Meshes generation
* [recast] Pathfinding (part of Recast, can use the generated data, or your own)
* [Swarmz] simple Boids, for flocking behaviors

<!-- Links  -->
[recast]:https://github.com/recastnavigation/recastnavigation
[Swarmz]:https://github.com/Cultrarius/Swarmz "A free, header-only C++ swarming (flocking) library for real-time applications" 

<!-- ############################### UTILITIES ############################### -->
# Utilities

*Tools to improve production.  
Resharper is free with an educationnal license and really adds a lot of productivity tools for your Visual Studio.  
Premake Helps for the .sln maintenance, no more merging issues in your solution when someone adds / remove code files.*

* [fastbuild]
* [cppcheck]
* [include-what-you-use]
* [Resharper] *`recommended`*
* [premake] *`recommended`*
* [GENie], Project generator tool

<!-- Links  -->
[fastbuild]:https://www.fastbuild.org/docs/features.html "Parallel compilation, distributed compilation, local caching, automatic blobbing. FASTBuild will speed up your compile Time drastically and is very easy to install"
[cppcheck]:http://cppcheck.sourceforge.net/ "Cppcheck is a static analysis tool for C/C++ code. It provides unique code analysis to detect bugs and focuses on detecting undefined behaviour and dangerous coding construct"
[include-what-you-use]:https://github.com/include-what-you-use/include-what-you-use "Analyze #includes in C and C++ source files, and removes useless ones. To clean your code, and make compilation faster"
[Resharper]:https://www.jetbrains.com/resharper/ "Visual studio Add-on for on the fly Code quality check, easy refactoring, code search options, automatic code standard enforcement."
[premake]:https://premake.github.io/ "Simple build configuration tool to automatically generate project/solution files"
[GENie]:https://github.com/bkaradzic/genie "Simple build configuration tool to automatically generate project/solution files, same as premake ;)"

<!-- ############################### FULL ENGINES ############################### -->
# Existing engines

*Existing 3D Engines with available sources, if you need inspiration ;)*

* [godot]
* [LumixEngine]
* [FlaxEngine]
* [Urho3D]
* [WickedEngine]

<!-- Links  -->
[godot]:https://github.com/godotengine/godot
[LumixEngine]:https://github.com/nem0/lumixengine
[FlaxEngine]:https://github.com/FlaxEngine/FlaxEngine
[Urho3D]:https://github.com/urho3d/Urho3D
[WickedEngine]:https://github.com/turanszkij/WickedEngine


<!-- ############################### GDC RESOURCES PROG ############################### -->
# GDC Resources for programmers

[GDC Programmer Playlist](https://www.youtube.com/playlist?list=PL2e4mYbwSTbaw1l65rE0Gv6_B9ctOzYyW "Full playlist of all the programming topic of the GDC Channel")

## Rendering
* [Water Rendering in Far Cry 5](https://youtu.be/4oDtGnQNCx4 "In this 2018 GDC session, AMD's Cristian Cutocheras and Ubisoft's Branislav Grujic discuss the Far Cry team's all-new water system that applies to a number of new water materials that allow the artists to create realistic water-based physical features such as waterfalls and lakes")
* [From Shore to Horizon: Creating a Practical Tessellation Based Solution](https://www.youtube.com/watch?v=MdC7L1OloKE "In this 2017 GDC talk, Eidos Montreal's Jean-Normand Bucci and Nicolas Longchamps discuss the techniques behind their Ocean Technology system. ")
* [The Latest Graphics Technology in Remedy's Northlight Engine](https://www.youtube.com/watch?v=0HeDjOeYqxI "In this 2018 GDC talk, Remedy's Taatu Aalto discusses the in-engine implementation and results of some recent advances in rendering technology in Remedy's Northlight engine.")
* [High Dynamic Range Color Grading and Display in Frostbite](https://www.youtube.com/watch?v=7z_EIjNG0pQ "In this 2017 GDC talk, EA's Alex Fry presents the approach the Frostbite team took to add support for HDR displays.")
* [Geometry Caching Optimizations in Halo 5: Guardians](https://www.youtube.com/watch?v=uYAjUOlEgwI "Geometry Caching Optimizations in Halo 5: Guardians")
* [The Illusion of Motion: Making Magic with Textures in the Vertex Shader](https://www.youtube.com/watch?v=EUTE1SoOGrk "In this 2017 GDC session, Tequila Works' Mario Palmero presents ways to use textures in vertex shaders as a cheaper alternative solution to established methods, and present a new technique for animation pipelines that bypasses the CPU and enables animators to increase bone counts dramatically.")
* [Global Illumination in Tom Clancy's The Division](https://www.youtube.com/watch?v=04YUZ3bWAyg "In this 2016 GDC talk, Ubisoft's Nikolay Stefanov describes the dynamic global illumination system that Ubisoft Massive created for Tom Clancy's The Division.")
* [Rendering Rainbow Six Siege](https://www.youtube.com/watch?v=RAy8UoO2blc "In this 2016 GDC session, Ubisoft Montreal's Jalal Eddine El Mansouri describes the most interesting work done by the R6 graphics team to ship Tom Clancy's Rainbow Six Siege on Xbox One, PS4 and up to 5 year old PCs, while focusing on architectural optimizations that leverages compute that are only possible with the current generation hardware")
* [Low Complexity, High Fidelity: The Rendering of INSIDE](https://www.youtube.com/watch?v=RdN06E6Xn9E "In this 2016 GDC Europe session, PlayDead's  Mikkel Gjoel and Mikkel Svendsen detail the techniques used to achieve high visual fidelity in the context of the uncompromisingly simplistic aesthetic of INSIDE, Playdead's follow-up to the critically acclaimed LIMBO. ")
* [Profiling with Intel Graphics Performance Analyzers](https://www.gdcvault.com/play/1026698/Does-Your-Game-s-Performance "Learn about Intel Graphics Performance Analyzers (Intel GPA) exciting new features to identify and quantify performance bottlenecks. We will demonstrate the synergy of using Graphics Frame Analyzer and Graphics Trace Analyzer to gain deeper insight into your game's execution. Using the new Intel GPA Framework, we will then describe how to automate portions of your profiling workflow and create reports for continuous integration systems and increase the visibility of your game's performance earlier in the development lifecycle.")


## AI
* [AI Arborist: Proper Cultivation and Care for Your Behavior Trees](https://www.youtube.com/watch?v=Qq_xX1JCreI "In this 2017 GDC talk, Bobby Anguelov, Mika Vehkala, and Ben Weber outline core principles to get the most out of your behavior trees while avoiding common issues.")
* [Tree's Company: Systemic AI Design in Just Cause 3](https://www.youtube.com/watch?v=SurYVTMINhg "In this 2017 GDC session, Avalanche Studios'  Robert Meyer details exactly what technical designs, interfaces, and methods Avalanche used to keep its AI systemic and robust, yet flexible and controllable enough for content creators.")
* [Hacking into the Combat AI of Watch Dogs 2](https://www.youtube.com/watch?v=c06DZ81Tbmk "In this 2017 GDC talk, Ubisoft's Chae Dickie-Clark shows how the NPCs of Watch Dogs 2 efficiently process their environment to select cover and perform other dynamic behaviors thanks to an intricate AI system designed by the Ubisoft team. ")
* [Taking Back What's Ours: The AI of Dishonored 2](https://www.youtube.com/watch?v=VoXSJBVqdek "In this 2017 GDC session, Arkane Studios' Laurent Couvidou and Xavier Sadoulet explain how the AI of Dishonored 2 was designed for a variety of systems including coordination of multiple NPCs, spatial reasoning for stealth and search, and a data-driven rule system for dialog and animations.")
* [AI & Animation in Assassin's Creed Brotherhood](https://www.youtube.com/watch?v=HzhDjbsXA9s "In this 2011 GDC session, Ubisoft's Nicolas Barbeau and Aleissia Laidacker discuss the AI and animation powering the virtual population of Rome in Assassin's Creed Brotherhood.")
* [Nuts and Bolts: Modular AI From the Ground Up](https://www.youtube.com/watch?v=IvK0ZlNoxjw "In this 2016 GDC panel, programmers Kevin Dill, Christopher Dragert & Troy Humphreys provide a comprehensive exploration of modular AI, from the theoretical underpinnings up to code examples from shipped titles.")
* [Modeling AI Perception and Awareness in Splinter Cell: Blacklist](https://www.youtube.com/watch?v=RFWrKHM0vAg "In this 2014 GDC talk, Ubisoft Toronto's Martin Walsh describes the AI stealth models used on Splinter Cell: Blacklist, the reason for using them, issues that came up, and Ubisoft's overall strategy for giving consistent feedback to the player while maintaining realism.")
* [Creating Complex AI Behavior in Stellaris Through Data-Driven Design](https://www.youtube.com/watch?v=Z5LMUbjyFQM "In this 2017 GDC talk, Paradox Interactive's Mehrnaz Amanat Bari covers the design and implementation of data driven AI in Stellaris to create NPCs with unique and non static personalities in a decision based game.")
* [Goal-Oriented Action Planning: Ten Years of AI Programming](https://www.youtube.com/watch?v=gm7K68663rA "In this 2015 GDC talk, AI Programmers Chris Conway, Peter Higley and Eric Jacopin revisit the Goal-Oriented Action Planning method of game AI programming to see how it's held up for the last 10 years, and how it influenced the AI of Middle Earth: Shadow of Mordor and Tomb Raider.")
* [Mind Your Step: Avoiding 3 Common Pitfalls in AI Development](https://www.youtube.com/watch?v=ZlShFdoAOxU "In this 2016 GDC session, Insomniac Games' Jan Mueller challenges 3 common misconceptions about best practices in AI design.") *`recommended`*
* [Tom Clancy's The Division: AI Behavior Editing and Debugging](https://www.youtube.com/watch?v=rYQQRIY_zcM "Tom Clancy's The Division features a large number of NPCs with a wide range of complex behaviors. The designers needed to be able to manage the creation, editing and debugging of the NPC AI in an effective, understandable and efficient way. This 2016 lecture from Massive Entertainment's Jonas Gillberg showa how this is made possible using the technology of the Snowdrop engine. This talk includes an introduction to how behavior trees are structured and how their logic works.")
* [Can You See Me Now? Building Robust AI Sensory Systems](https://www.youtube.com/watch?v=pUXS9snrZE4 "In this 2017 GDC talk, Ubisoft's Eric Martel describes how to construct advanced sensory systems to allow AI to better respond to player inputs. ")
* [Virtual Insanity: Meta AI on Assassin's Creed: Origins](https://www.youtube.com/watch?v=a09vnDjmY_E "In this 2018 GDC session, Ubisoft's Charles Lefebvre discusses the creation of Meta AI for Assassin's Creed: Origins and how its objects could be virtual or real, in formations, in conflict, be part of a mission or autonomous, and how they can have a persistent inventory.")
* [Knowledge is Power: An Overview of Knowledge Representation in Game AI](https://www.youtube.com/watch?v=Z6oZnDIgio4 "In this 2018 GDC talk, Daniel Brewer and Rez Graham explain best practices for helping video game AI make decisions that will feel impactful to the player. ")
* 

## Physics
* [Physics for Game Programmers: Understanding Constraints](https://archive.org/details/GDC2014Catto "This session will be about writing a character controller. Erin will show how to implement a character controller using swept collision and an iterative solver. The solver naturally handles collision priorities using Lagrange multipliers.")
* [Supercharged! Vehicle Physics in Skylanders](https://www.youtube.com/watch?v=Db1AgGavL8E "In this 2016 GDC talk, Vicarious Visions' Jan Erik Steel & Patrick Donnelly  cover how land, sea and air physics handling for Skylanders Superchargers was created, exploring how they managed the complexities of the physical simulation while always striving for a simple and intuitive designer interface and tools.")
* [Physics for Game Programmers; Continuous Collision](https://www.youtube.com/watch?v=7_nKOET6zwI "In this 2013 GDC session, Blizzard Entertainment's Erin Catto explores how to solve collision problems in game programming.")
* [It IS Rocket Science! The Physics of Rocket League Detailed](https://www.youtube.com/watch?v=ueEmiDM94IE "In this 2018 GDC talk, Psyonix's Jared Cone takes viewers through an inside look at the specific game design decisions and implementation details that made the networked physics of Rocket League so successful.") *`recommended`*

## Gameplay
* [Math for Game Programmers: Juicing Your Cameras With Math](https://www.youtube.com/watch?v=tu-Qe66AvtY) *`recommended`*
* [	Techniques for Building Aim Assist in Console Shooters](https://www.gdcvault.com/play/1017942/Techniques-for-Building-Aim-Assist "This talk will present an overview of the systems and techniques used to create the aiming controls for Insomniac Games' Resistance 3. The talk will cover basic camera movement topics like camera acceleration and deadzoning, as well as aim assist systems such as magnetism, centering, and friction. The talk will include how these systems interacted with the mission structure and first-person shooter gameplay.")
* [Math for Game Programmers: Predictable Projectiles](https://www.youtube.com/watch?v=6OkhjWUIUf0 "In this 2017 GDC talk, Robot Entertainment's Chris Stark explains the math and code behind predictive linear and ballistic projectile aiming, how to handle practical variations, and best practices for exposing knobs to designers.")
  
## Others
* [Animation Bootcamp: An Indie Approach to Procedural Animation](https://www.youtube.com/watch?v=LNidsMesxSE "In this 2014 GDC session, indie developer David Rosen explains how to use simple procedural techniques to achieve interactive and fluid animations using very few key frames, with examples from indie games like Overgrowth, Receiver and Black Shades." ) *`recommended`*
* [Stress-Free Game Development: Powering Up Your Studio With DevOps](https://www.youtube.com/watch?v=0jMOONXg29c "In this 2020 GDC Virtual Talk, Butterscotch Shenanigans' Seth Coster walks through how his team learned to use DevOps to get more done while working less.")
* [Overwatch Gameplay Architecture and Netcode](https://www.youtube.com/watch?v=W3aieHjyNvw "In this 2017 GDC session, Blizzard's Timothy Ford explains how Overwatch uses the Entity Component System (ECS) architecture to create a rich variety of layered gameplay.") *`recommended`*
* [I Shot You First: Networking the Gameplay of Halo: Reach](https://www.youtube.com/watch?v=h47zZrqjgLc "In this 2011 GDC session, Bungie's David Aldridge discusses the programming that drove Halo: Reach's online networking. ")
* [Replicating Chaos: Vehicle Replication in Watch Dogs 2](https://www.youtube.com/watch?v=_8A2gzRrWLk "In this 2017 GDC session, Ubisoft Toronto's Matt Delbosc explores the techniques used in Watch Dogs 2 to replicate vehicle trajectories, compensate for network latency, and realistically represent vehicle-versus-vehicle collisions between players.")

<!-- ############################### GDC RESOURCES DESIGN ############################### -->
# GDC Resources for designers
Interesting talks for Designers


## Animations
* [12 Principles of Animation](https://www.youtube.com/watch?v=uDqjIdI4bF4) *`recommended`*
* [Satisfying Reloads | The Art in Reload Animations](https://www.youtube.com/watch?v=ym_mNU6DWK8)
* [Animation Bootcamp: The First Person Animation of Overwatch](https://www.youtube.com/watch?v=7t0hLZd_8Z4)
* [How Overwatch Conveys Character in First Person](https://www.youtube.com/watch?v=7Dga-UqdBR8)
* [The Art of Destiny's First-Person Animation](https://www.youtube.com/watch?v=CXFLu8cityA)
* [How to Animate a Smash Bros Attack](https://www.youtube.com/watch?v=3kTYazhO3fs)
* [How Great First-Person Animations are Made - 10 Tips for Animating FPS Characters/Weapons in Blender](https://www.youtube.com/watch?v=dclA9iwZB_s)
* [The Best Animation Tricks of the Trade (For 2016)](https://www.youtube.com/watch?v=_1j5Tf6ulII)


## Camera
<!-- Camera Design  -->
* [50 Game Camera Mistakes](https://www.youtube.com/watch?v=C7307qRmlMI) *`recommended`*
* [Creating an Emotionally Engaging Camera for Tomb Raider](https://www.youtube.com/watch?v=doVivf-Nvuo)

## UI
* [Developing a UX Mindset on Fortnite](https://www.gdcvault.com/play/1026946/Developing-a-UX-Mindset-on "	This GDC 2019 talk will feature real-time interaction with the speaker through online chat messaging during its scheduled broadcast. While there is no recipe for crafting successful video games, there are specific ingredients that we can rely on and a specific process that can guide developers along the way. The ingredients come from our understanding of the human brain. The process comes from the scientific approach and from design thinking. Considering them both allows developers to adopt a user experience (UX) mindset for their project, empowering them to accomplish their goals faster and more efficiently. Former director of UX at Epic Games explains in this talk how such UX strategy was developed on Fortnite")

## Design
<!-- Overall design  -->
* [30 Things I Hate About Your Game Pitch](https://www.youtube.com/watch?v=4LTtr45y7P0) *`recommended`*


# Books recommendations *(programmers)*
* `Game Engine Architecture, Third Edition` ,ISBN-13: 978-1138035454
* `Head First Design Patterns: A Brain-Friendly Guide` , ISBN-13: 978-0596007126
* `Game Programming Algorithms and Techniques: A Platform-Agnostic Approach (Game Design)` ,ISBN-13: 978-0321940155
* `Game Programming Patterns` ,ISBN-13: 978-0990582908
* `3D Math Primer for Graphics and Game Development` ,ISBN-13: 978-1568817231
* `Physics for Game Developers: Science, math, and code for realistic effects` ,ISBN-13: 978-1449392512


# Free eBooks recommendations
* [2D Game Development: From Zero To Hero](https://therealpenaz91.itch.io/2dgd-f0th) *`MUST READ !`* Free eBook, all the knowledge and tutorials to make a 2D game from scratch *(basically GAM200)* 
* [GameAIPro](http://www.gameaipro.com/) Different articles on AI related articles (FSM, Pathfinding, FPS bots, crowds...) for games
* [Procedural Generation for Games](http://pcgbook.com/)
* [Game Programming Partterns](http://gameprogrammingpatterns.com/) Free version of the book listed in the book section

---
>Contact me via PM to add / remove content